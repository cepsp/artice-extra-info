<?php

namespace Eol\ArticleExtraInfo;

use Illuminate\Support\ServiceProvider;

class ArticleExtraInfoServiceProvider extends ServiceProvider
{
    /**
     * 命令的 namespace 路径
     * @var array
     */
    protected $commands = [
    ];

    //加载路由和迁移
    function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/Routes/routes.php');
        $this->loadMigrationsFrom(__DIR__ . '/Migrations');
    }

    //注册命令
    function register()
    {
        $this->commands($this->commands);
    }
}
